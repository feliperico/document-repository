package br.com.touchtec.repository;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

/**
 * Created by feliperico on 13/10/15.
 */
public class DocumentTest {

    @Test
    public void buildDocumentSuccessfully() {
        Document.Builder builder = new Document.Builder("novo");
        Date creationDate = new Date();
        Document document = builder
                .withCategory("categoria")
                .createdIn(creationDate)
                .withId("123")
                .withPath("/root/")
                .build();
        Assert.assertNotNull(document);
        Assert.assertEquals("123", document.getId());
        Assert.assertEquals("novo", document.getName());
        Assert.assertEquals("/root/", document.getPath());
        Assert.assertEquals("categoria", document.getCategory());
        Assert.assertFalse(document.hasChildren());
        Assert.assertFalse(document.isFolder());
        Assert.assertTrue(document.isDocument());
        Assert.assertFalse(document.hasData());
        Assert.assertFalse(document.isDataAvailable());
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenBuildingDocumentWithoutNameExceptionIsThrown() {
        Document repoNode = new Document.Builder("").build();
    }
}
