package br.com.touchtec.repository.utils;

import org.infinispan.AdvancedCache;
import org.infinispan.Cache;
import org.infinispan.lifecycle.ComponentStatus;
import org.infinispan.manager.CacheContainer;
import org.infinispan.manager.EmbeddedCacheManager;

import javax.transaction.TransactionManager;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by feliperico on 13/10/15.
 */
public class TestUtil {

    /**
     * Stops a collection of ISPN caches and stops any running transactions.
     *
     * @param caches a collection of caches
     */
    public static void killCaches( Iterable<Cache<?, ?>> caches ) {
        for (Cache<?, ?> c : caches) {
            killCache(c);
        }
    }

    /**
     * Stops an ISPN cache
     *
     * @param c the cache to stop
     */
    public static void killCache( Cache<?, ?> c ) {
        try {
            if (c != null && c.getStatus() == ComponentStatus.RUNNING) {
                AdvancedCache advancedCache = c.getAdvancedCache();
                if (advancedCache != null) {
                    TransactionManager tm = advancedCache.getTransactionManager();
                    if (tm != null) {
                        try {
                            tm.rollback();
                        } catch (Exception e) {
                            // don't care
                        }
                    }
                }
                c.stop();
            }
        } catch (Throwable t) {
            t.printStackTrace(System.err);
        }
    }

    /**
     * Stops an array of cache managers.
     *
     * @param containers an array of {@link EmbeddedCacheManager}
     */
    public static void killCacheContainers( CacheContainer... containers ) {
        for (CacheContainer container : containers) {
            if (!(container instanceof EmbeddedCacheManager) || ((EmbeddedCacheManager)container)
                    .getStatus() != ComponentStatus.RUNNING) {
                continue;
            }
            EmbeddedCacheManager manager = (EmbeddedCacheManager)container;
            Set<Cache<?, ?>> caches = new HashSet<Cache<?, ?>>();
            for (String cacheName : manager.getCacheNames()) {
                Cache<Object, Object> cache = manager.getCache(cacheName, false);
                if (cache != null) {
                    AdvancedCache<Object, Object> advancedCache = cache.getAdvancedCache();
                    caches.add(advancedCache);
                }
            }
            killCaches(caches);
        }
        for (CacheContainer cm : containers) {
            try {
                if (cm != null) { cm.stop(); }
            } catch (Throwable e) {
                e.printStackTrace(System.err);
            }
        }
    }

}
