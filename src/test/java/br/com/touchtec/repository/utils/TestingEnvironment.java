package br.com.touchtec.repository.utils;

import org.infinispan.configuration.cache.Configuration;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.global.GlobalConfigurationBuilder;
import org.infinispan.manager.CacheContainer;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.transaction.TransactionMode;
import org.infinispan.transaction.lookup.DummyTransactionManagerLookup;
import org.infinispan.transaction.lookup.TransactionManagerLookup;
import org.modeshape.jcr.LocalEnvironment;

/**
 * Created by feliperico on 13/10/15.
 */
public class TestingEnvironment extends LocalEnvironment {

    public TestingEnvironment() {
        this(DummyTransactionManagerLookup.class);
    }


    public TestingEnvironment( Class<? extends TransactionManagerLookup> transactionManagerLookup ) {
        super(transactionManagerLookup);
    }

    @Override
    protected void shutdown( CacheContainer container ) {
        TestUtil.killCacheContainers(container);
    }

    @Override
    protected Configuration createDefaultConfiguration() {
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        configurationBuilder.transaction().transactionMode(TransactionMode.TRANSACTIONAL);
        configurationBuilder.transaction().transactionManagerLookup(transactionManagerLookupInstance());
        return configurationBuilder.build();
    }

    @Override
    protected CacheContainer createContainer( GlobalConfigurationBuilder globalConfigurationBuilder,
                                              ConfigurationBuilder configurationBuilder ) {
        configurationBuilder.jmxStatistics().disable();
        globalConfigurationBuilder.globalJmxStatistics().disable().allowDuplicateDomains(true);
        return new DefaultCacheManager(globalConfigurationBuilder.build(), configurationBuilder.build(), true);
    }
}
