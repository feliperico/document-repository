package br.com.touchtec.repository;

import br.com.touchtec.repository.api.DocumentFilter;
import br.com.touchtec.repository.exception.DocumentRepositoryException;
import br.com.touchtec.repository.modeshape.DocumentRepositoryImpl;
import br.com.touchtec.repository.utils.MultiUseAbstractTest;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.modeshape.jcr.JcrSession;

import java.util.List;

/**
 * Created by feliperico on 13/10/15.
 */
public class DocumentRepositoryImplTest extends MultiUseAbstractTest {

    public DocumentRepositoryImpl documentRepository;

    @Before
    public void beforeEach() throws Exception {
        documentRepository = new DocumentRepositoryImpl(null);
        documentRepository.setRepository(repository);
        session = (JcrSession)documentRepository.getSession();
        registerNodeTypes("nodes.cnd");

    }

    @After
    public void afterEach() throws Exception {
        try {
            documentRepository.getSession().logout();
        } finally {
            session = null;
        }
    }

    @Test
    public void createAndRetrieveDocumentInDefaultLocation() {
        String fileName = "testFile1.txt";
        Document.Builder docBuilder = new Document.Builder(fileName);
        docBuilder.withCategory("categoria")
                .withDataStream(resourceStream(fileName));
        Document doc = documentRepository.put(docBuilder.build());
        Assert.assertNotNull(doc.getId());
        Assert.assertNotNull(documentRepository.findById(doc.getId()));
    }

    @Test
    public void createAndRetrieveDocumentInCustomLocation() {
        String fileName = "testFile1.txt";
        Document.Builder docBuilder = new Document.Builder(fileName);
        docBuilder.withCategory("categoria")
                .withDataStream(resourceStream(fileName))
                .withPath("/my/custom/path");
        Document doc = documentRepository.put(docBuilder.build());
        Assert.assertNotNull(doc.getId());
        Assert.assertNotNull(documentRepository.findById(doc.getId()));
    }

    @Test
    public void createAndRemoveDocument() {
        String fileName = "testFile1.txt";
        Document.Builder docBuilder = new Document.Builder("testFile2.txt");
        docBuilder.withCategory("categoria")
                .withDataStream(resourceStream(fileName));
        Document doc = documentRepository.put(docBuilder.build());
        Assert.assertNotNull(doc.getId());
        documentRepository.remove(doc);
        Assert.assertNull(documentRepository.findById(doc.getId()));
    }

    @Test(expected = DocumentRepositoryException.class)
    public void exceptionOnRemovingRepositoryNodeWithChildren() throws DocumentRepositoryException {
        String fileName = "testFile1.txt";
        Document.Builder docBuilder = new Document.Builder("testFile3.txt");
        docBuilder.withCategory("categoria")
                .withDataStream(resourceStream(fileName))
                .withPath("/my/custom/path");
        documentRepository.put(docBuilder.build());
        RepositoryNode repositoryNode = documentRepository.getNodeByAbsolutePath("/my/custom/path");
        Assert.assertNotNull(repositoryNode);
        documentRepository.remove(repositoryNode);
    }

    @Test
    public void retrieveRepositoryNodeChildren() throws DocumentRepositoryException {
        String fileName = "testFile1.txt";
        Document.Builder docBuilder = new Document.Builder("testFile4.txt");
        docBuilder.withCategory("categoria")
                .withDataStream(resourceStream(fileName))
                .withPath("/my/custom/path");
        documentRepository.put(docBuilder.build());
        RepositoryNode repositoryNode = documentRepository.getNodeByAbsolutePath("/my/custom/path");
        Assert.assertNotNull(repositoryNode);
        List<RepositoryNode> repoNodes = documentRepository.getChildrenNodes(repositoryNode.getId());
        Assert.assertFalse(repoNodes.isEmpty());
    }

    @Test
    public void removeRepositoryNodeWithNoChildren() throws DocumentRepositoryException {
        String fileName = "testFile1.txt";
        Document.Builder docBuilder = new Document.Builder("testFile5.txt");
        docBuilder.withCategory("categoria")
                .withDataStream(resourceStream(fileName))
                .withPath("/my/custom/path");
        Document doc = documentRepository.put(docBuilder.build());
        documentRepository.remove((RepositoryNode)doc);
        Assert.assertNull(documentRepository.findById(doc.getId()));
    }

    @Test
    public void findByNameExactAndPrefix() throws DocumentRepositoryException {
        String fileName = "testFile1.txt";
        Document.Builder docBuilder = new Document.Builder("testFile6.txt");
        docBuilder.withCategory("categoria")
                .withDataStream(resourceStream(fileName))
                .withPath("/my/custom/path")
                .withCategory("nova categoria");
        Document doc = documentRepository.put(docBuilder.build());

        List<Document> docs = documentRepository.find(DocumentFilter.equal("name", "testFile6.txt"));
        Assert.assertEquals(1, docs.size());

        docs = documentRepository.find(DocumentFilter.like("category", "nova cat%"));
        Assert.assertEquals(1, docs.size());
    }

}
