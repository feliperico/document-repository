package br.com.touchtec.repository;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

/**
 * Created by feliperico on 13/10/15.
 */
public class RepositoryNodeTest {

    @Test
    public void buildRepositoryNodeSuccessfully() {
        RepositoryNode.Builder builder = new RepositoryNode.Builder("novo");
        Date creationDate = new Date();
        RepositoryNode repoNode = builder.asFolder()
                .createdIn(creationDate)
                .hasChildren()
                .withId("123")
                .withPath("/root/")
                .build();
        Assert.assertNotNull(repoNode);
        Assert.assertEquals("123", repoNode.getId());
        Assert.assertEquals("novo", repoNode.getName());
        Assert.assertEquals("/root/", repoNode.getPath());
        Assert.assertTrue(repoNode.hasChildren());
        Assert.assertTrue(repoNode.isFolder());
        Assert.assertFalse(repoNode.isDocument());
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenBuildingRepositoryNodeWithoutNameExceptionIsThrown() {
        RepositoryNode repoNode = new RepositoryNode.Builder("").build();
    }

}
