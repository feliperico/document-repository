<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="applicationPath" value="${pageContext.request.contextPath}"/>

<t:main pageTitle="Repositório" >

  <jsp:attribute name="styles">
    <link href="${applicationPath}/static/css/jstree/themes/default/style.min.css" rel="stylesheet">
  </jsp:attribute>
  <jsp:attribute name="scripts">
      <script src="${applicationPath}/static/js/lib/jstree.min.js"></script>
      <script src="${applicationPath}/static/js/commons.js"></script>
      <script>
        applicationPath = "${applicationPath}";
      </script>
      <script src="${applicationPath}/static/js/repoview.js"></script>
  </jsp:attribute>

  <jsp:body>
    <div class="page-header text-center">
      <h1>Repositório</h1>
      <p class="lead">
          Utilize a árvore abaixo para visualizar a estrutura do repositório de arquivos.
          Clique com o botão direito do mouse sobre os nós para ver detalhes.
      </p>
    </div>
    <div class="row">
      <div class="col-sm-offset-3 col-md-6">
        <div id="repo-tree"></div>
      </div>
    </div>
    <jsp:include page="details.jsp"/>
  </jsp:body>
</t:main>