<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<t:main pageTitle="Principal" >
    <jsp:body>
        <!-- Título -->
        <div class="jumbotron">
            <h1>ModeShape POC</h1>
            <p class="lead">Prova de conceito do modeshape. Use o menu acima para navegar entre as funcionalidades.</p>
        </div>
    </jsp:body>
</t:main>