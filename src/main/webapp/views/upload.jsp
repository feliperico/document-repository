<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="applicationPath" value="${pageContext.request.contextPath}"/>

<t:main pageTitle="Upload" >
    <jsp:body>
        <!-- Título -->
        <div class="page-header text-center">
            <h1>Upload</h1>
            <p class="lead">Preencha os campos abaixo. Nome e arquivo são campos obrigatórios.</p>
        </div>
        <c:if test="${not empty message}">
        <div class="row">
            <div class="col-sm-offset-3 col-md-6">
                <div class="alert ${messageType eq 'SUCCESS' ? 'alert-success' : 'alert-danger'}" role="alert">
                    ${message}
                </div>
            </div>
        </div>
        </c:if>
        <div class="row">
            <div class="col-sm-offset-3 col-md-6">
                <form method="POST" action="${applicationPath}/upload/" enctype="multipart/form-data" accept-charset="UTF-8">
                    <div class="form-group">
                        <label for="name">Nome</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Nome do arquivo no repositório">
                    </div>
                    <div class="form-group">
                        <label for="path">Caminho</label>
                        <input type="text" class="form-control" id="path" name="path" placeholder="Ex.: /meu/caminho/personalizado">
                    </div>
                    <div class="form-group">
                        <label for="category">Categoria</label>
                        <input type="text" class="form-control" id="category" name="category" placeholder="Categoria do arquivo">
                    </div>
                    <div class="form-group">
                        <label for="file">Arquivo</label>
                        <input type="file" id="file" name="file">
                        <p class="help-block">Selecione um arquivo.</p>
                    </div>
                    <button type="submit" class="btn btn-default">Enviar</button>
                </form>
            </div>
        </div>
    </jsp:body>
</t:main>