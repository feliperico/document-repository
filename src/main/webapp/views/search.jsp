<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="applicationPath" value="${pageContext.request.contextPath}"/>

<t:main pageTitle="Busca" >

  <jsp:attribute name="scripts">
      <script src="${applicationPath}/static/js/commons.js"></script>
      <script>
        applicationPath = "${applicationPath}";
      </script>
      <script src="${applicationPath}/static/js/search.js"></script>
  </jsp:attribute>

  <jsp:body>
    <div class="page-header text-center">
      <h1>Busca</h1>
      <p class="lead">Preencha os campos abaixo para buscar documentos no repositório.</p>
    </div>
    <div class="row">
      <div class="col-sm-offset-3 col-md-6">
        <div class="form-inline">
          <div class="form-group">
            <label for="search-property">Busca por</label>
            <select id="search-property" class="form-control">
                <option value="name" >Nome</option>
                <option value="category" >Categoria</option>
                <option value="content" >Conteúdo</option>
            </select>
          </div>
          <div class="form-group">
            <label class="sr-only" for="search-value">Valor</label>
            <input type="text" class="form-control" id="search-value" placeholder="Valor">
          </div>
          <button id="search-button" class="btn btn-default">Busca</button>
        </div>
      </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Data Criação</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody id="search-result-body">

                </tbody>
            </table>
        </div>
    </div>

      <jsp:include page="details.jsp" />
  </jsp:body>
</t:main>