<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="applicationPath" value="${pageContext.request.contextPath}"/>

<t:main pageTitle="Download" >

  <jsp:attribute name="scripts">
      <script src="${applicationPath}/static/js/commons.js"></script>
      <script>
          applicationPath = "${applicationPath}";
      </script>
      <script src="${applicationPath}/static/js/download.js"></script>
  </jsp:attribute>

  <jsp:body>
    <div class="page-header text-center">
      <h1>Download</h1>
      <p class="lead">Preencha o campo abaixo com o id do arquivo para baixá-lo.</p>
    </div>
    <div class="row">
      <div class="col-sm-offset-3 col-md-6">
          <div class="form-inline">
              <div class="form-group">
                  <label for="document-identifier">Arquivo</label>
                  <input type="text" class="form-control" id="document-identifier" placeholder="Id do arquivo no repositório">
              </div>
              <button id="download-button" class="btn btn-default">Download</button>
          </div>
      </div>
    </div>
  </jsp:body>
</t:main>