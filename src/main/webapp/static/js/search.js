/**
 * Created by feliperico on 15/10/15.
 */
$(function () {
    $("#search-button").click(function() {
        var property = $("#search-property option:selected").attr("value");
        var value = $("#search-value").val();
        if (property && value) {
            $.getJSON(applicationPath + "/search-by/" + property,
                {"value": value},
                function(data) {
                    if ($.isArray(data)){
                        var resultBody = $("#search-result-body");
                        resultBody.empty();
                        $.each(data, function (index, item) {
                            getResultRow(item).appendTo(resultBody);
                        });
                    }
                }
            );
        }
    });

    function getResultRow(node) {
        var row = $("<tr/>");
        $("<td/>", {"text": node.name}).appendTo(row);
        $("<td/>", {"text": node.creationDate}).appendTo(row);
        var actionsColumn = $("<td/>");
        var buttonDownload = $("<button/>",{
            "type": "button",
            "class": "btn btn-default",
            "alt": "Download",
            "click" : function () {
                downloadDocument(node.id);
            }
        });
        buttonDownload.append($("<span/>", {"class": "glyphicon glyphicon-download-alt", "aria-hidden":"true"}));
        buttonDownload.appendTo(actionsColumn);
        var buttonDetails = $("<button/>",{
            "type": "button",
            "class": "btn btn-default",
            "alt": "Propriedades",
            "click" : function () {
                showNodeAttributes(node);
            }
        });
        buttonDetails.append($("<span/>", {"class": "glyphicon glyphicon-th-list", "aria-hidden":"true"}));
        buttonDetails.appendTo(actionsColumn);

        actionsColumn.appendTo(row);
        return row;
    }
});