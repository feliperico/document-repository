$(function () {
    $("#repo-tree").jstree({
        "plugins": ["sort", "contextmenu"],
        "core" : {
            "data" : {
                "url" : function (node) {
                    return node.id === "#" ?
                        applicationPath + "/children-nodes/root" :
                        applicationPath + "/children-nodes/" + node.id;
                }
            }
        },
        "contextmenu": {
            "items": function (node, callback) {
                var actions = {};
                // Mostra atributos do nó
                actions["attributes"] = {
                    "label": "Propriedades",
                    "action": function() {
                        showNodeAttributes(node.original);
                    }
                };
                if (node.original.type === "DOCUMENT") {
                    // Permite fazer o download quando é doc
                    actions["download"] = {
                        "label": "Download",
                        "action": function() {
                            downloadDocument(node.id);
                        }
                    }
                }
                callback(actions);
            }
        }
    });
});