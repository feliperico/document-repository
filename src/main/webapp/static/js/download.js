/**
 * Created by feliperico on 14/10/15.
 */
$(function () {
    $("#download-button").click(function () {
        var documentId = $("#document-identifier").val();
        if (documentId) {
            downloadDocument(documentId);
        }
    });
})