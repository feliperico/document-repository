/**
 * Created by feliperico on 15/10/15.
 */

/**
 * Global que guarda o caminho de acesso à aplicação.
 * Setado pelo jsp do servidor
 * @type {string}
 */
var applicationPath = "";

/**
 * Exibe os detalhes de um nó no repositório
 *
 * @param node objeto json com os dados do nó
 */
function showNodeAttributes(node) {
    if (node.attributes) {
        var modalContainer = $("#attr-modal");
        $("#attr-modal-title", modalContainer).text(node.text);
        var modalBody = $("#attr-modal-body", modalContainer);
        modalBody.empty();
        $.each(node.attributes, function(name, value) {
            var content = "<b>" + name + ":</b> " + $("<span/>", {"text": value}).html();
            var attr = $("<p/>", {"html": content});
            attr.appendTo(modalBody);
        });
        modalContainer.modal("show");
    }
}

/**
 * Faz o download do documento
 *
 * @param documentId identificador único do documento no repositório
 */
function downloadDocument(documentId) {
    var iframeId = "download-iframe";
    if (documentId) {
        var iframe = $("#"+iframeId);
        if (iframe.size() == 0) {
            // cria iframe caso não exista
            iframe = $("<iframe/>", {id: iframeId, style: "display:none;"})
                .appendTo($("body"));
        }
        iframe.attr("src", applicationPath + "/download/" + documentId);
    }
}