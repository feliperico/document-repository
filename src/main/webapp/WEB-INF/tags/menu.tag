<%@tag description="Main Template" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@attribute name="selected" required="true" %>

<c:set var="applicationPath" value="${pageContext.request.contextPath}"/>

<ul class="nav navbar-nav">
    <li class="${selected eq 'Principal' ? 'active' : ''}">
        <a href="${applicationPath}/">Principal</a>
    </li>
    <li class="${selected eq 'Upload' ? 'active' : ''}">
        <a href="${applicationPath}/upload/">Upload</a>
    </li>
    <li class="${selected eq 'Download' ? 'active' : ''}">
        <a href="${applicationPath}/downpage/">Download</a>
    </li>
    <li class="${selected eq 'Repositório' ? 'active' : ''}">
        <a href="${applicationPath}/repoview/">Repositório</a>
    </li>
    <li class="${selected eq 'Busca' ? 'active' : ''}">
        <a href="${applicationPath}/search/">Busca</a>
    </li>
</ul>