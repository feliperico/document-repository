<%@tag description="Main Template" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@attribute name="styles" fragment="true" %>
<%@attribute name="scripts" fragment="true" %>
<%@attribute name="pageTitle"  %>

<c:set var="applicationPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="pt">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <c:if test="${empty pageTitle}">
    <c:set var="pageTitle" value="Principal" />
  </c:if>
  <title>Modeshape POC | ${pageTitle}</title>

  <!-- Bootstrap -->
  <link href="${applicationPath}/static/css/bootstrap.min.css" rel="stylesheet">
  <link href="${applicationPath}/static/css/bootstrap-theme.min.css" rel="stylesheet">
  <link href="${applicationPath}/static/css/styles.css" rel="stylesheet">

  <jsp:invoke fragment="styles" />

  <!-- jQuery -->
  <script src="${applicationPath}/static/js/lib/jquery.min.js" ></script>
  <!-- Bootstrap script -->
  <script src="${applicationPath}/static/js/lib/bootstrap.min.js" ></script>
</head>

<body role="document">
<div class="container">
    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Modeshape POC</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <t:menu selected="${pageTitle}" />
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>

    <jsp:doBody/>

</div><!--/.container -->


<jsp:invoke fragment="scripts" />
</body>
</html>
