package br.com.touchtec.repository.api;

/**
 * Enum contendo as especificações dos repositórios disponíveis
 * 
 * @author felipe.rico
 * 
 */
public enum RepositorySpec {
    DEFAULT("Repository1", "repository1.json");

    private String name;
    private String configurationFile;

    private RepositorySpec(String name, String configurationFile) {
        this.name = name;
        this.configurationFile = configurationFile;
    }

    public String getName() {
        return name;
    }

    public String getConfigurationFile() {
        return configurationFile;
    }
}
