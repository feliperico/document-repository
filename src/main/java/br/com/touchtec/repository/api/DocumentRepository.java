package br.com.touchtec.repository.api;

import br.com.touchtec.repository.Document;
import br.com.touchtec.repository.RepositoryNode;
import br.com.touchtec.repository.exception.DocumentRepositoryException;

import java.util.List;

/**
 * API de um repositório de documentos com estrutura hierárquica em árvore
 * (semelhante a um File System).
 *
 * Created by feliperico on 09/10/15.
 */
public interface DocumentRepository {

    /**
     * Procura um documento pelo seu id no repositório.
     *
     * @param id identificador único do documento no repositório
     * @return {@link Document} encontrado ou {@code null} caso contrário.
     */
    Document findById(String id);

    /**
     * Procura os nós filhos de um nó com o id fornecido.
     * Útil para situações onde deseja-se navegar pela estrutura do repositório
     * sem efetivamente carregar os dados dos documentos (apenas os metadados).
     *
     * @param id identificador único do nó no repositório.
     * @return lista de {@link RepositoryNode} filhos ou lista vazia caso não existam.
     */
    List<RepositoryNode> getChildrenNodes(String id);

    /**
     * Procura o nó do repositório pelo seu caminho absoluto no repositório de documentos.
     *
     * @param absolutePath caminho absoluto no repositório
     * @return {@link RepositoryNode} encontrado ou null caso não exista.
     */
    RepositoryNode getNodeByAbsolutePath(String absolutePath);

    /**
     * Salva ou atualiza um documento no repositório.
     *
     * @param document documento a ser salvo
     */
    Document put(Document document);

    /**
     * Remove um documento específico do repositório de documentos.
     *
     * @param document {@link Document} a ser removido.
     */
    void remove(Document document);

    /**
     * Apaga um dado nó do repositório de documentos.
     *
     * @param node {@link RepositoryNode} existente no repositório
     *
     * @throws DocumentRepositoryException quando tenta-se excluir um nó que possui filhos (evitando-se remoção recursiva)
     */
    void remove(RepositoryNode node) throws DocumentRepositoryException;

    /**
     * Procura por documentos no repositório.
     * Retorna {@link Document}'s sem os streams de dados;
     *
     * @param filter {@link DocumentFilter} a ser usado na busca
     * @return coleção de documentos encontrados com o filtro especificado
     */
    List<Document> find(DocumentFilter filter);
}
