package br.com.touchtec.repository.api;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Representa um filtro para buscar documentos no repositório
 *
 * Created by feliperico on 15/10/15.
 */
public class DocumentFilter {

    private static final String EQUAL_OPERATOR = "%s = '%s'";
    private static final String LIKE_OPERATOR = "%s LIKE '%s'";
    private static final String CONTAINS_OPERATOR = "CONTAINS(%s, '%s')";
    private static final String OR_OPERATOR = " OR ";
    private static final String AND_OPERATOR = " AND ";
    private static final String QUERY_TEMPLATE = "SELECT doc.* FROM [touch:doc] AS doc " +
            "JOIN [nt:resource] AS resource ON ISCHILDNODE(resource, doc) WHERE %s";
    private static final Map<String, String> REAL_PROP_NAMES;
    static {
        REAL_PROP_NAMES = new HashMap<>();
        REAL_PROP_NAMES.put("name", "doc.[jcr:name]");
        REAL_PROP_NAMES.put("category", "doc.[touch:category]");
        REAL_PROP_NAMES.put("content", "resource.[jcr:data]");
    }

    private String property;
    private String value;
    private String operator;
    private DocumentFilter[] filters;

    private DocumentFilter(String property, String value, String operator) {
        this.property = property;
        this.operator = operator;
        this.value = value;
    }

    private DocumentFilter(DocumentFilter[] documentFilters, String operator) {
        this.operator = operator;
        this.filters = documentFilters;
    }

    public static DocumentFilter equal(String property, String value) {
        return new DocumentFilter(property, value, EQUAL_OPERATOR);
    }

    public static DocumentFilter like(String property, String value) {
        return new DocumentFilter(property, value, LIKE_OPERATOR);
    }

    public static DocumentFilter or(DocumentFilter... documentFilters) {
        return new DocumentFilter(documentFilters, OR_OPERATOR);
    }

    public static DocumentFilter and(DocumentFilter... documentFilters) {
        return new DocumentFilter(documentFilters, AND_OPERATOR);
    }

    public static DocumentFilter contains(String property, String value) {
        return new DocumentFilter(property, value, CONTAINS_OPERATOR);
    }

    public String getQueyString() {
        return String.format(QUERY_TEMPLATE, getWhereExpresion());
    }

    private String getWhereExpresion() {
        if (filters != null && filters.length > 0) {
            StringBuilder sbExpression = new StringBuilder();
            String prefix = "";
            for (DocumentFilter filter : filters) {
                if (filter.isValid()) {
                    sbExpression.append(prefix)
                            .append("( ")
                            .append(filter.getWhereExpresion())
                            .append(" )");
                }
                prefix = operator;
            }
            return sbExpression.toString();
        } else {
            String propName;
            if (REAL_PROP_NAMES.containsKey(property)) {
                propName = REAL_PROP_NAMES.get(property);
            } else {
                propName = "doc." + property;
            }
            return String.format(operator, propName, value);
        }
    }

    public boolean isValid() {
        return (filters != null && filters.length > 0)
                || (StringUtils.isNotBlank(property) && StringUtils.isNotBlank(value));
    }

}
