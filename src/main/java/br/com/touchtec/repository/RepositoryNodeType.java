package br.com.touchtec.repository;

/**
 * Representa o tipo de nó que pertence ao repositório de arquivos.
 *
 * Created by feliperico on 09/10/15.
 */
public enum RepositoryNodeType {
    DOCUMENT,
    FOLDER,
    OTHER;

    public static boolean isDocument(RepositoryNodeType type) {
        if (type != null) {
            return type.equals(RepositoryNodeType.DOCUMENT);
        }
        return false;
    }

    public static boolean isFolder(RepositoryNodeType type) {
        if (type != null) {
            return type.equals(RepositoryNodeType.FOLDER);
        }
        return false;
    }
}
