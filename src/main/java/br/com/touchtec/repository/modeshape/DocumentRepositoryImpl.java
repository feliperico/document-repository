package br.com.touchtec.repository.modeshape;

import br.com.touchtec.repository.Document;
import br.com.touchtec.repository.RepositoryNode;
import br.com.touchtec.repository.api.DocumentFilter;
import br.com.touchtec.repository.api.DocumentRepository;
import br.com.touchtec.repository.api.RepositorySpec;
import br.com.touchtec.repository.exception.DocumentRepositoryException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.modeshape.common.collection.Problems;
import org.modeshape.jcr.JcrRepository;
import org.modeshape.jcr.ModeShapeEngine;
import org.modeshape.jcr.NoSuchRepositoryException;
import org.modeshape.jcr.RepositoryConfiguration;

import javax.jcr.*;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Implementação do {@link br.com.touchtec.repository.api.DocumentRepository}
 * usando a API JCR com a implementação Modeshape.
 *
 * Created by feliperico on 09/10/15.
 */
public class DocumentRepositoryImpl implements DocumentRepository {
    public static final Logger LOG = Logger.getLogger(DocumentRepositoryImpl.class);

    private static final String DOCUMENT_NODE_TYPE = "touch:doc";
    private static final String FOLDER_NODE_TYPE = "nt:folder";
    private static final String RESOURCE_NODE_TYPE = "nt:resource";
    private static final String CATEGORY_PROP = "touch:category";
    private static final String CONTENT_NODE = "jcr:content";
    private static final String DATA_PROP = "jcr:data";
    private static final String CREATED_PROP = "jcr:created";
    private static final String MIME_TYPE_PROP = "jcr:mimeType";
    private static final String ROOT_ID = "root";

    private static final ModeShapeEngine ENGINE = new ModeShapeEngine();

    private static final String DEFAULT_FOLDER_NAME = "/public";

    private RepositorySpec currentRepoSpec = RepositorySpec.DEFAULT;

    private Repository repository;

    private Session _currSession;

    public DocumentRepositoryImpl(RepositorySpec repositorySpec) {
        if (repositorySpec != null) {
            initializeRepository(repositorySpec);
        }
    }

    /**
     * Inicializa o repositório se o mesmo já não estiver inicializado.
     *
     * @param repoSpec
     *            Especificação do repositório que deve ser incializado
     */
    public void initializeRepository(RepositorySpec repoSpec) {
        // Garante-se a inicialização da engine
        startEngine();

        if (!currentRepoSpec.equals(repoSpec)) {
            currentRepoSpec = repoSpec;
        }

        // Verifica se o repositório já foi inicializado
        if (!isRepositoryDeployed(repoSpec)) {
            deployRepository();
        } else try {
            // verifica se o repositório está em execução
            JcrRepository repo = ENGINE.getRepository(repoSpec.getName());
            if (!repo.getState().equals(ModeShapeEngine.State.RUNNING)) {
                // Inicializa o repositório novamente
                Future<Boolean> undeploy = ENGINE.undeploy(repoSpec.getName());
                if (undeploy.get()) {
                    deployRepository();
                }
            }
            repository = repo;
        } catch (NoSuchRepositoryException | InterruptedException | ExecutionException e) {
            LOG.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    /**
     * verifica se o repositório já sofreu o deploy na engine.
     *
     * @param repoSpec especificação do repositório
     * @return boolean indicando <tt>true</tt> se foi instalado
     */
    private boolean isRepositoryDeployed(RepositorySpec repoSpec) {
        String repoName = repoSpec.getName();
        Map<String, ModeShapeEngine.State> deployedRepos = ENGINE.getRepositories();
        for (Map.Entry<String, ModeShapeEngine.State> repoEntry : deployedRepos.entrySet()) {
            if (repoEntry.getKey().equals(repoName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Faz o deploy do repositório na engine
     *
     */
    private void deployRepository() {
        try {
            // Caso não tenha sido, inicializa-o
            RepositoryConfiguration config = RepositoryConfiguration.read(currentRepoSpec.getConfigurationFile());

            // Verifica-se por problemas de configuração do repositório
            Problems problems = config.validate();
            if (problems.hasErrors()) {
                // Reporta-se os erros caso existam
                LOG.warn("Problemas com a configuração do repositório.");
                LOG.warn(problems);
            }

            // Faz-se o deploy se a configuração estiver funcional
            javax.jcr.Repository repo = ENGINE.deploy(config);

            // Procura-se por problemas na inicialização que não sejam
            // impedimentos
            problems = ((JcrRepository) repo).getStartupProblems();
            if (problems.hasErrors() || problems.hasWarnings()) {
                // Reporta-se os erros caso existam...
                LOG.error("Problems deploying the repository.");
                LOG.error(problems);
                return;
            }

            // Seta o repositório corrente
            repository = repo;
        } catch (Exception e) {
            LOG.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    /**
     * Inicializa a engine se a mesma já não estiver rodando
     */
    private void startEngine() {
        if (!ENGINE.getState().equals(ModeShapeEngine.State.RUNNING)) {
            ENGINE.start();
        }
    }

    /**
     * Interrompe a execução da engine
     */
    public void stopEngine() {
        ENGINE.shutdown();
    }

    /**
     * Adquire a sessão corrente
     *
     * @return objeto Session
     */
    public Session getSession() {
        if (_currSession == null || !_currSession.isLive()) {
            startSession();
        }
        return _currSession;
    }

    /**
     * Inicializa a sessão com o repositório corrente
     */
    public void startSession() {
        try {
            _currSession = repository.login();
        } catch (RepositoryException e) {
            LOG.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public Document findById(String id) {
        if (StringUtils.isNotBlank(id)) {
            try {
                Node docNode = getSession().getNodeByIdentifier(id);
                return fromNodeToDocument(docNode, true);
            } catch (RepositoryException e) {
                LOG.debug(e.getMessage(), e);
            } finally {
                getSession().logout();
            }
        }
        return null;
    }

    private RepositoryNode fromNodeToRepositoryNode(Node node) {
        try {
            RepositoryNode.Builder repoNodeBuilder = new RepositoryNode.Builder(node.getName());
            repoNodeBuilder.withId(node.getIdentifier())
                    .withPath(node.getParent().getPath());

            if (node.getNodes().getSize() > 0l) {
                repoNodeBuilder.hasChildren();
            }

            if ( node.hasProperty(CREATED_PROP) ) {
                repoNodeBuilder.createdIn(node.getProperty(CREATED_PROP).getDate().getTime());
            }

            PropertyIterator propIter = node.getProperties();
            while (propIter.hasNext()) {
                Property property = propIter.nextProperty();
                try {
                    String value = property.getValue().getString();
                    repoNodeBuilder.addAttribute(property.getName(), value);
                } catch (Exception e) {
                    // ignora
                }
            }

            if (node.isNodeType(DOCUMENT_NODE_TYPE)) {
                repoNodeBuilder.asDocument();
                repoNodeBuilder.noChildren();
            } else if (node.isNodeType(FOLDER_NODE_TYPE)) {
                repoNodeBuilder.asFolder();
            }

            return repoNodeBuilder.build();
        } catch (RepositoryException e) {
            LOG.debug(e.getMessage(), e);
        }
        return null;
    }

    private Document fromNodeToDocument(Node node, boolean loadData) {
        try {
            if (node.isNodeType(DOCUMENT_NODE_TYPE)) {
                Document.Builder docBuilder = new Document.Builder(node.getName());
                docBuilder
                        .withPath(node.getParent().getPath())
                        .withId(node.getIdentifier())
                        .createdIn(node.getProperty(CREATED_PROP).getDate().getTime());
                if ( node.hasProperty(CATEGORY_PROP) ) {
                    docBuilder.withCategory(node.getProperty(CATEGORY_PROP).getString());
                }

                PropertyIterator propIter = node.getProperties();
                while (propIter.hasNext()) {
                    Property property = propIter.nextProperty();
                    try {
                        String value = property.getValue().getString();
                        docBuilder.addAttribute(property.getName(), value);
                    } catch (Exception e) {
                        // ignora
                    }
                }

                if (loadData) {
                    // TODO verificar melhor maneira de fazer isso
                    // copia o stream de dados para não travar o repositório ao dar logout
                    Node docContent = node.getNode(CONTENT_NODE);
                    Binary binary = docContent.getProperty(DATA_PROP).getBinary();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    IOUtils.copy(binary.getStream(), baos);
                    ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
                    docBuilder.withDataStream(bais);
                    if ( docContent.hasProperty(MIME_TYPE_PROP) ) {
                        docBuilder.withMimeType(docContent.getProperty(MIME_TYPE_PROP).getString());
                    }
                }
                return docBuilder.build();
            }
        } catch (RepositoryException | IOException e) {
            LOG.debug(e.getMessage(), e);
        }
        return null;
    }

    public List<RepositoryNode> getChildrenNodes(String id) {
        if (StringUtils.isNotBlank(id)) {
            try {
                Node node;
                if (id.equals(ROOT_ID)) {
                    node = getSession().getRootNode();
                } else {
                    node = getSession().getNodeByIdentifier(id);
                }
                List<RepositoryNode> repositoryNodes = new ArrayList<>();
                NodeIterator iter = node.getNodes();
                while (iter.hasNext()) {
                    Node child = iter.nextNode();
                    repositoryNodes.add(fromNodeToRepositoryNode(child));
                }
                return repositoryNodes;
            } catch (RepositoryException e) {
                LOG.debug(e.getMessage(), e);
            } finally {
                getSession().logout();
            }
        }
        return Collections.emptyList();
    }

    public Document put(Document document) {
        if (document != null) {
            if (isIdValid(document.getId())) {
                // TODO atualiza o documento no repositório
            } else {
                // Cria o documento no repositório
                Node node = createNodeFromDocument(document);
                document = fromNodeToDocument(node, false);
            }
            getSession().logout();
        }
        return document;
    }

    private Node createNodeFromDocument(Document document) {
        try {
            Node location = getLocationNode(document.getPath());

            Node docNode = location.addNode(document.getName(), DOCUMENT_NODE_TYPE);
            if (StringUtils.isNotBlank(document.getCategory())) {
                docNode.setProperty(CATEGORY_PROP, document.getCategory());
            }

            if (document.hasData() && document.isDataAvailable()) {
                Node contentNode = docNode.addNode(CONTENT_NODE, RESOURCE_NODE_TYPE);
                Binary binary = getSession().getValueFactory().createBinary(document.getDataStream());
                contentNode.setProperty(DATA_PROP, binary);
            }

            getSession().save();
            return docNode;
        } catch (RepositoryException e) {
            LOG.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    private Node getLocationNode(String path) throws RepositoryException {
        Node location = null;
        if (StringUtils.isNotBlank(path)) {
            //
            String[] pathArray = path.split("/");
            Node currNode = getSession().getRootNode();
            for (int i = 1; i < pathArray.length; i++) {
                String folder = pathArray[i];
                try {
                    location = currNode.getNode(folder);
                } catch (PathNotFoundException e) {
                    location = createFolder(currNode, folder);
                }
                currNode = location;
            }
        } else {
            // Retorna o local padrão
            try {
                location = getSession().getNode(DEFAULT_FOLDER_NAME);
            } catch (PathNotFoundException e) {
                location = createDefaultFolder();
            }

        }
        return location;
    }

    private boolean isIdValid(String id) {
        if (StringUtils.isNotBlank(id)) {
            try {
                Node node = getSession().getNodeByIdentifier(id);
                return node != null;
            } catch (RepositoryException e) {
                LOG.info(e.getMessage());
            }
        }
        return false;
    }

    private Node createDefaultFolder() throws RepositoryException {
        Node defaultFolder;
        Node root = getSession().getRootNode();
        defaultFolder = root.addNode(DEFAULT_FOLDER_NAME, FOLDER_NODE_TYPE);
        getSession().save();
        return defaultFolder;
    }

    private Node createFolder(Node parent, String folderName) throws RepositoryException {
        Node node = parent.addNode(folderName, FOLDER_NODE_TYPE);
        getSession().save();
        return node;
    }

    public void remove(Document document) {
        if (document != null && StringUtils.isNotBlank(document.getId())) {
            try {
                Node node = getSession().getNodeByIdentifier(document.getId());
                if (node.isNodeType(DOCUMENT_NODE_TYPE)) {
                    node.remove();
                    getSession().save();
                }
            } catch (RepositoryException e) {
                LOG.debug(e.getMessage(), e);
            } finally {
                getSession().logout();
            }
        }
    }

    public void remove(RepositoryNode repoNode) throws DocumentRepositoryException {
        if (repoNode != null && StringUtils.isNotBlank(repoNode.getId())) {
            try {
                Node node = getSession().getNodeByIdentifier(repoNode.getId());
                if (node.isNodeType(DOCUMENT_NODE_TYPE) ||
                        (!node.isNodeType(DOCUMENT_NODE_TYPE) && node.getNodes().getSize() == 0l)) {
                    node.remove();
                    getSession().save();
                } else {
                    throw new DocumentRepositoryException("Nó possui filhos!");
                }
            } catch (RepositoryException e) {
                LOG.debug(e.getMessage(), e);
                throw new DocumentRepositoryException(e);
            } finally {
                getSession().logout();
            }
        }
    }

    public RepositoryNode getNodeByAbsolutePath(String absolutePath) {
        if (StringUtils.isNotBlank(absolutePath)) {
            try {
                Node node = getSession().getNode(absolutePath);
                return fromNodeToRepositoryNode(node);
            } catch (RepositoryException e) {
                LOG.debug(e.getMessage(), e);
            } finally {
                getSession().logout();
            }
        }
        return null;
    }

    public List<Document> find(DocumentFilter filter) {
        if (filter != null && filter.isValid()) {
            try {
                QueryManager queryManager = getSession().getWorkspace().getQueryManager();
                Query query = queryManager.createQuery(filter.getQueyString(), javax.jcr.query.Query.JCR_SQL2);
                QueryResult result = query.execute();
                NodeIterator nodeIter = result.getNodes();
                List<Document> docs = new ArrayList<>();
                while (nodeIter.hasNext()) {
                    Node node = nodeIter.nextNode();
                    docs.add(fromNodeToDocument(node, false));
                }
                return docs;
            } catch (RepositoryException e) {
                LOG.error(e.getMessage(), e);
                throw new RuntimeException(e);
            } finally {
                getSession().logout();
            }
        }
        return Collections.emptyList();
    }

    public void setRepository(Repository repository) {
        this.repository = repository;
    }

}
