package br.com.touchtec.repository.exception;

/**
 * Created by feliperico on 09/10/15.
 */
public class DocumentRepositoryException extends Exception {

    public DocumentRepositoryException(String message) {
        super(message);
    }

    public DocumentRepositoryException(String message, Throwable cause) {
        super(message, cause);
    }

    public DocumentRepositoryException(Throwable cause) {
        super(cause);
    }

    public DocumentRepositoryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
