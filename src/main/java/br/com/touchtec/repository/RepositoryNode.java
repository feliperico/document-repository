package br.com.touchtec.repository;

import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Representa um nó da árvore do repositório de arquivos.
 *
 * Created by feliperico on 09/10/15.
 */
public class RepositoryNode {

    protected String id;
    protected String name;
    protected String path;
    protected boolean children;
    protected Date creationDate;
    protected Map<String, String> attributes;
    protected RepositoryNodeType type;

    protected RepositoryNode() {}

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public boolean hasChildren() {
        return children;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public boolean isDocument() {
        return RepositoryNodeType.isDocument(type);
    }

    public boolean isFolder() {
        return RepositoryNodeType.isFolder(type);
    }

    public boolean hasData() {
        return false;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public static class Builder {
        private RepositoryNode instance;

        public Builder(String nodeName) {
            if (StringUtils.isNotBlank(nodeName)) {
                getInstance().name = nodeName;
            } else {
                throw new IllegalArgumentException("nodeName must not be null or an empty string.");
            }
        }

        public Builder withName(String name) {
            getInstance().name = name;
            return this;
        }

        public Builder withId(String id) {
            getInstance().id = id;
            return this;
        }

        public Builder withPath(String path) {
            getInstance().path = path;
            return this;
        }

        public Builder hasChildren() {
            getInstance().children = true;
            return this;
        }

        public Builder noChildren() {
            getInstance().children = false;
            return this;
        }

        public Builder asDocument() {
            getInstance().type = RepositoryNodeType.DOCUMENT;
            return this;
        }

        public Builder asFolder() {
            getInstance().type = RepositoryNodeType.FOLDER;
            return this;
        }

        public Builder createdIn(Date date) {
            getInstance().creationDate = date;
            return this;
        }

        public Builder addAttribute(String name, String value) {
            if (getInstance().attributes == null) {
                getInstance().attributes = new HashMap<>();
            }
            getInstance().attributes.put(name, value);
            return this;
        }

        public RepositoryNode build() {
            return getInstance();
        }

        protected RepositoryNode getInstance() {
            if (instance == null) {
                instance = new RepositoryNode();
                instance.type = RepositoryNodeType.OTHER;
            }
            return instance;
        }
    }
}
