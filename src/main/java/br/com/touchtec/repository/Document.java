package br.com.touchtec.repository;


import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;

/**
 * Representa um arquivo(documento) do repositório de arquivos.
 *
 * Created by feliperico on 09/10/15.
 */
public class Document extends RepositoryNode {

    private InputStream dataStream;

    private boolean dataAvailable;

    private String category;

    private String mimeType;

    protected Document() {}

    public void writeTo(OutputStream outputStream) throws IOException {
        if (outputStream != null && dataAvailable) {
            IOUtils.copy(dataStream, outputStream);
            dataStream.close();
            outputStream.close();
            dataAvailable = false;
        }
    }

    public boolean isDataAvailable() {
        return dataAvailable;
    }

    public String getCategory() {
        return category;
    }

    public String getMimeType() {
        return mimeType;
    }

    @Override
    public boolean hasData() {
        return dataStream != null;
    }

    public InputStream getDataStream() {
        return dataStream;
    }

    public static class Builder {

        private Document instance;

        public Builder(String nodeName) {
            if (StringUtils.isNotBlank(nodeName)) {
                getInstance().name = nodeName;
            } else {
                throw new IllegalArgumentException("nodeName must not be null or an empty string.");
            }
        }

        public Builder withName(String name) {
            getInstance().name = name;
            return this;
        }

        public Builder withId(String id) {
            getInstance().id = id;
            return this;
        }

        public Builder withPath(String path) {
            getInstance().path = path;
            return this;
        }

        public Builder asDocument() {
            getInstance().type = RepositoryNodeType.DOCUMENT;
            return this;
        }

        public Builder createdIn(Date date) {
            getInstance().creationDate = date;
            return this;
        }

        public Document build() {
            return getInstance();
        }

        public Builder withDataStream(InputStream dataStream) {
            if (dataStream != null) {
                getInstance().dataStream = dataStream;
                getInstance().dataAvailable = true;
            }
            return this;
        }

        public Builder withCategory(String category) {
            getInstance().category = category;
            return this;
        }

        public Builder withMimeType(String mimeType) {
            getInstance().mimeType = mimeType;
            return this;
        }

        public Builder addAttribute(String name, String value) {
            if (getInstance().attributes == null) {
                getInstance().attributes = new HashMap<>();
            }
            getInstance().attributes.put(name, value);
            return this;
        }

        protected Document getInstance() {
            if (instance == null) {
                instance = new Document();
                instance.type = RepositoryNodeType.DOCUMENT;
                instance.children = false;
            }
            return instance;
        }
    }
}
