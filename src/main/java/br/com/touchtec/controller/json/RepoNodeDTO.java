package br.com.touchtec.controller.json;

import br.com.touchtec.repository.Document;
import br.com.touchtec.repository.RepositoryNode;
import br.com.touchtec.repository.RepositoryNodeType;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * POJO para geração do json de visualização da estrutura do diretório de documentos.
 *
 * Created by feliperico on 14/10/15.
 */
public class RepoNodeDTO {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    private String id;
    private String text;
    private String icon;
    private String path;
    private Date creationDate;
    private boolean children;
    private String type;
    private Map<String, String> attributes;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getCreationDate() {
        if (creationDate != null){
            return DATE_FORMAT.format(creationDate);
        }
        return null;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public boolean isChildren() {
        return children;
    }

    public void setChildren(boolean children) {
        this.children = children;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public String getName() {
        return text;
    }

    public static RepoNodeDTO fromRepositoryNode(RepositoryNode repositoryNode) {
        if (repositoryNode != null) {
            RepoNodeDTO dto = new RepoNodeDTO();
            dto.setId(repositoryNode.getId());
            dto.setChildren(repositoryNode.hasChildren());
            dto.setCreationDate(repositoryNode.getCreationDate());
            dto.setText(repositoryNode.getName());
            dto.setPath(repositoryNode.getPath());
            dto.setAttributes(repositoryNode.getAttributes());
            if (repositoryNode.isDocument()) {
                dto.setIcon("glyphicon glyphicon-file");
                dto.setType(RepositoryNodeType.DOCUMENT.name());
            } else if (repositoryNode.isFolder()) {
                dto.setIcon("glyphicon glyphicon-folder-open");
                dto.setType(RepositoryNodeType.FOLDER.name());
            } else {
                dto.setIcon("glyphicon glyphicon-question-sign");
                dto.setType(RepositoryNodeType.OTHER.name());
            }
            return dto;
        }
        return null;
    }

    public static List<RepoNodeDTO> fromRepositoryNodes(Collection<RepositoryNode> repositoryNodes) {
        if (!repositoryNodes.isEmpty()) {
            List<RepoNodeDTO> dtos = new ArrayList<>();
            for (RepositoryNode repositoryNode : repositoryNodes) {
                dtos.add(fromRepositoryNode(repositoryNode));
            }
            return dtos;
        }
        return Collections.emptyList();
    }

    public static List<RepoNodeDTO> fromDocuments(Collection<Document> documents) {
        if (!documents.isEmpty()) {
            List<RepoNodeDTO> dtos = new ArrayList<>();
            for (RepositoryNode repositoryNode : documents) {
                dtos.add(fromRepositoryNode(repositoryNode));
            }
            return dtos;
        }
        return Collections.emptyList();
    }
}
