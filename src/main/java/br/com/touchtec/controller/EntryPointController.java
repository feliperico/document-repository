package br.com.touchtec.controller;

import br.com.touchtec.controller.json.RepoNodeDTO;
import br.com.touchtec.repository.Document;
import br.com.touchtec.repository.api.DocumentFilter;
import br.com.touchtec.repository.api.DocumentRepository;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;

@Controller
public class EntryPointController {
    public static final Logger LOG = Logger.getLogger(EntryPointController.class);

    @Autowired
    private DocumentRepository documentRepository;

    @RequestMapping("/")
    public String getIndex() {
        return "index";
    }

    @RequestMapping(value = "/upload", method = RequestMethod.GET)
    public String getUploadPage() {
        return "upload";
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String uploadFileHandler(@RequestParam("name") String name,
                                    @RequestParam("category") String category,
                                    @RequestParam("path") String path,
                                    @RequestParam("file") MultipartFile file,
                                    Model model) {

        if (!file.isEmpty() && StringUtils.isNotBlank(name)) {
            try {
                Document.Builder docBuilder = new Document.Builder(name);
                Document doc = docBuilder.withCategory(category)
                        .withPath(path)
                        .withDataStream(file.getInputStream())
                        .build();

                doc = documentRepository.put(doc);
                model.addAttribute("messageType", "SUCCESS");
                model.addAttribute("message", "Upload concluído com sucesso! Id do arquivo no repositório: " + doc.getId());
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
                model.addAttribute("messageType", "ERROR");
                model.addAttribute("message", "Ocorreu um erro ao salvar o arquivo no repositório: " + e.getMessage());
            }
        } else {
            model.addAttribute("messageType", "ERROR");
            model.addAttribute("message", "O arquivo e o nome são campos obrigatórios!");
        }

        return "upload";
    }

    @RequestMapping(value = "/downpage", method = RequestMethod.GET)
    public String getDownloadPage() {
        return "downpage";
    }

    @RequestMapping(value = "/download/{documentId}", method = RequestMethod.GET)
    public void downloadFile(@PathVariable String documentId, HttpServletResponse response) {

        if (StringUtils.isNotBlank(documentId)) {
            Document doc = documentRepository.findById(documentId);
            if (doc != null) {
                if (StringUtils.isNotBlank(doc.getMimeType())) {
                    response.setContentType(doc.getMimeType());
                }

                String headerKey = "Content-Disposition";
                String headerValue = String.format("attachment; filename=\"%s\"",
                        doc.getName());
                response.setHeader(headerKey, headerValue);
                try {
                    doc.writeTo(response.getOutputStream());
                } catch (IOException e) {
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    LOG.error(e.getMessage(), e);
                }
            }
        }
    }

    @RequestMapping(value = "/repoview", method = RequestMethod.GET)
    public String getRepoViewPage() {
        return "repoview";
    }

    @RequestMapping(value = "/children-nodes/{repositoryNodeId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Collection<RepoNodeDTO> downloadFile(@PathVariable String repositoryNodeId) {
        if (StringUtils.isNotBlank(repositoryNodeId)) {
            return RepoNodeDTO.fromRepositoryNodes(documentRepository.getChildrenNodes(repositoryNodeId));
        }
        return Collections.emptyList();
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String getSearchPage() {
        return "search";
    }

    @RequestMapping(value = "/search-by/{propertyName}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Collection<RepoNodeDTO> searchByProperty(@PathVariable String propertyName, @RequestParam("value") String value) {
        if (StringUtils.isNotBlank(propertyName) && StringUtils.isNotBlank(value)) {
            DocumentFilter filter;
            if (propertyName.equals("content")) {
                filter = DocumentFilter.contains(propertyName, value);
            } else {
                filter = DocumentFilter.like(propertyName, "%" + value + "%");
            }
            return RepoNodeDTO.fromDocuments(documentRepository.find(filter));
        }
        return Collections.emptyList();
    }
}
